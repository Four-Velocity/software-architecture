"""Project configuration."""

from typing import Union, Dict, Any

from pathlib import Path
import yaml


def get_config(path: Union[Path, str]) -> Dict[str, Any]:
    """
    Loads config from yaml file.

    Args:
        path: path to yaml file.

    Returns:
        a parsed config.
    """
    with open(path) as cfg_file:
        cfg = yaml.safe_load(cfg_file)
    return cfg


BASE_DIR = Path(__file__).parent.parent
PROJECT_DIR = BASE_DIR / "src"

dirs_config = dict(
    BASE_DIR=BASE_DIR,
    PROJECT_DIR=PROJECT_DIR,
    TEMPLATES_DIR=PROJECT_DIR / "templates",
    STATIC_DIR=PROJECT_DIR / "static",
)

config = get_config(BASE_DIR / "config" / "server.yaml")
config.update(dirs_config)
