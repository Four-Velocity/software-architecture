"""Web Server Views."""

from aiohttp import web

__all__ = [
    "IndexView",
    "CameraDetailView",
    "CameraReceiverView",
    "routes",
]

routes = web.RouteTableDef()


@routes.view("/")
class IndexView(web.View):
    """
    Main page view.

    Display all available cameras.
    """

    async def get(self):
        """Render all cameras."""
        pass


@routes.view("/{camera_id}")
class CameraDetailView(web.View):
    """
    Detail camera view.

    Displays camera images and logs.
    Allows turn on/off camera and change other settings.
    """

    async def get(self):
        """Render camera's details."""
        pass

    async def post(self):
        """Proceed form."""
        pass


@routes.view("/receiver")
class CameraReceiverView(web.View):
    """Cameras' requests receiver."""

    async def post(self):
        """Proceed data."""
        pass
