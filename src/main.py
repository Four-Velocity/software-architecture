"""Web Server Entrypoint."""

from aiohttp import web

from views import routes
from settings import config
from db.connections import *

if __name__ == "__main__":
    app = web.Application()
    app["config"] = config
    app.add_routes(routes)
    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)
    web.run_app(app)
