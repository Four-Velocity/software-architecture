from sqlalchemy import create_engine, MetaData

from settings import config
from db.models import *

DSN = "postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"


def create_tables(engine):
    """Create tables from models."""
    meta = MetaData()
    meta.create_all(bind=engine, tables=[camera, picture, log_message])


if __name__ == "__main__":
    db_url = DSN.format(**config["postgres"])
    engine = create_engine(db_url)

    create_tables(engine)
