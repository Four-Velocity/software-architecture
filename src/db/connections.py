"""PostgreSQL async connection initialization and closing."""

from aiohttp.web import Application
import aiopg.sa

__all__ = ["init_pg", "close_pg"]


async def init_pg(app: Application):
    """Add psql engine to app."""
    conf = app["config"]["postgres"]
    engine = await aiopg.sa.create_engine(
        database=conf["db_name"],
        user=conf["db_user"],
        password=conf["db_password"],
        host=conf["db_host"],
        port=conf["db_port"],
        minsize=conf["minsize"],
        maxsize=conf["maxsize"],
    )
    app["db"] = engine


async def close_pg(app: Application):
    """Remove psql engine from an app and close connection."""
    app["db"].close()
    await app["db"].wait_closed()
