"""Database models."""

import enum
from sqlalchemy import (
    MetaData,
    Table,
    Column,
    ForeignKey,
    Text,
    Integer,
    String,
    DateTime,
    Boolean,
    Enum,
)

__all__ = ["camera", "picture", "log_message"]

meta = MetaData()


class _CameraLogLevelChoices(enum.Enum):
    """Choices for camera's `log_level`."""

    DEBUG = 5
    INFO = 4
    WARN = 3
    ERROR = 2
    CRIT = 1


camera = Table(
    "camera",
    meta,
    Column("id", Integer, primary_key=True),
    Column("ip", String(15), unique=True),
    Column("location", String(32), nullable=False),
    Column("is_on", Boolean, unique=False, default=True),
    Column("log_level", Enum(_CameraLogLevelChoices), default=2, nullable=True),
)

# Pictures from camera
picture = Table(
    "picture",
    meta,
    Column("id", Integer, primary_key=True),
    Column("name", String(256), unique=True),
    Column("extension", String(5), nullable=False),
    Column("path", String(256), nullable=False),
    Column("made_at", DateTime, nullable=False),
    Column("expired_at", DateTime, nullable=True),
    Column("camera_id", Integer, ForeignKey("camera.id", ondelete="CASCADE")),
)

# Cameras' logs
log_message = Table(
    "log_message",
    meta,
    Column("id", Integer, primary_key=True),
    Column("date_time", DateTime, nullable=False),
    Column("log_level", Enum(_CameraLogLevelChoices), nullable=False),
    Column("text", Text),
    Column("camera_id", Integer, ForeignKey("camera.id", ondelete="CASCADE")),
)
